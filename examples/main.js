import Vue from 'vue';
import Bulma from '../src/index';
import App from './App';
import 'bulma/bulma.sass';
import  router from './router/index';

import "../packages/styles/sass/bulma.sass"

Vue.use(Bulma);

Vue.config.productionTip = false

new Vue({
    render: h => h(App),
    router
}).$mount('#app');
