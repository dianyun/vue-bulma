
const  version = '1.0';
const components = [

];

const install = function(Vue) {
    components.map(component => {
        Vue.component(component.name, component);
    })
}


/* istanbul ignore if */
if (typeof window !== 'undefined' && window.Vue) {
    install(window.Vue);
}

export {
    version,
    install
};

export default {
    install,
    version
};

